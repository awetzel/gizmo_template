#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 (2.9 useable) GB per core, 192 GB total
#SBATCH --job-name=gizmo
##SBATCH --partition=development  # 2 hours, 40 nodes (2240 cores), 1 job
#SBATCH --partition=normal  # 2 days, 512 nodes (28,672 cores), 50 jobs
##SBATCH --partition=large  # 2 days, 513-2048 node (114,688 cores), 5 jobs
#SBATCH --nodes=4
#SBATCH --tasks-per-node=56    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=48:00:00
#SBATCH --output=gizmo_jobs/gizmo_job_%j.txt
#SBATCH --mail-user=arwetzel@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin
#SBATCH --account=AST21010

'''
Submit a Gizmo simulation job to the SLURM queue.
Check if restart files exist - if yes, start a restart run.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io

# whether restart should be from snapshot file
restart_from_snapshot = False

# names of files and directories
executable_file_name = './gizmo/GIZMO'  # executable file
parameter_file_name = 'gizmo_parameters.txt'  # parameter file

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

# check if input id of previous job that needs to finish before this starts
SubmissionScript.check_existing_job('submit_gizmo_frontera.py', submit_before=False, time_delay=4)

# set execution command
execute_command = f'ibrun {executable_file_name} {parameter_file_name}'
# check if restart
execute_command += SubmissionScript.get_restart_flag(restart_from_snapshot)
# set output files
execute_command += ' 1> gizmo.out 2> gizmo.err'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()

# execute
os.system(execute_command)

SubmissionScript.print_runtime()
