#!/usr/bin/env python3

# job name
#PBS -N music
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
#PBS -q devel
##PBS -q normal
##PBS -q long
# cpu configuration
# select = nodes; ncpus = cores per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Haswell (node has 24 cores, 5.3 GB per core, 128 GB total)
#PBS -l select=1:ncpus=24:mpiprocs=1:ompthreads=24:model=has
## Broadwell node: 28 cores, 128 GB
##PBS -l select=1:ncpus=28:mpiprocs=1:ompthreads=28:model=bro
#PBS -l walltime=2:00:00
# combine stderr and stdout into one file
#PBS -j oe
# output file name
##PBS -o music_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -m ae
#PBS -M arwetzel@gmail.com
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit a MUSIC job to the PBS queue.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''


import os
import glob

from utilities import io as ut_io

# parallelization parameters
node_number = 1  # number of nodes
mpi_number_per_node = 1  # number of MPI tasks per node

# names of files
executable = os.environ['HOME'] + '/local/music/MUSIC'
parameter_file_name_base = 'ic_*.conf'

# get whatever parameter file exists in this directory
parameter_file_name = glob.glob(parameter_file_name_base)
assert len(parameter_file_name) == 1
parameter_file_name = parameter_file_name[0]

# move to directory am in when submit this job
os.chdir(os.environ['PBS_O_WORKDIR'])

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('pbs', node_number, mpi_number_per_node)

# execute
execute_command = f'{executable} {parameter_file_name}'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()
os.system(execute_command)

SubmissionScript.print_runtime()
