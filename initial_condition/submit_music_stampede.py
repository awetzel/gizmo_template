#!/usr/bin/env python3.11

# Stampede2
# SKX node: 48 cores, 4.0 (3.5 useable) GB per core, 192 GB total
# ICX node: 80 cores, 3.2 (3.0 useable) GB per core, 256 GB total
#SBATCH --job-name=music
#SBATCH --partition=skx-dev
##SBATCH --partition=skx-normal
##SBATCH --partition=icx-normal
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    ## MPI tasks per node
#SBATCH --cpus-per-task=48    ## OpenMP threads per MPI task
#SBATCH --time=2:00:00
#SBATCH --output=music_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-AST140064

'''
Submit MUSIC job to queue.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''


import os
import glob

from utilities import io as ut_io

# names of files
executable = os.environ['HOME'] + '/local/music/MUSIC'
parameter_file_name_base = 'ic_*.conf'

# get whatever parameter file exists in this directory
parameter_file_name = glob.glob(parameter_file_name_base)
assert len(parameter_file_name) == 1
parameter_file_name = parameter_file_name[0]

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

execute_command = f'{executable} {parameter_file_name}'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()
os.system(execute_command)

SubmissionScript.print_runtime()
