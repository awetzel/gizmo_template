#!/usr/bin/env python3.11

# Frontera
# normal node: 56 cores, 3.4 GB per core, 192 GB total
# Cascade Lake node: 112 cores, 18.7 GB per core, 2.1 TB total
#SBATCH --job-name=music
##SBATCH --partition=development  # 2 hours, 40 nodes (2240 cores), 1 job
##SBATCH --partition=normal  # 2 days, 512 nodess (28,672 cores), 50 jobs
##SBATCH --partition=large  # 2 days, 513-2048 nodes (114,688 cores), 5 jobs
#SBATCH --partition=nvdimm  # 2 days, 4 nodes, 2 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    ## MPI tasks per node
#SBATCH --cpus-per-task=112    ## OpenMP threads per MPI task
#SBATCH --time=12:00:00
#SBATCH --output=music_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit a MUSIC job to queue.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import glob

from utilities import io as ut_io

# names of files
executable = os.environ['HOME'] + '/local/music/MUSIC'
parameter_file_name_base = 'ic_*.conf'

# get whatever parameter file exists in this directory
parameter_file_name = glob.glob(parameter_file_name_base)
assert len(parameter_file_name) == 1
parameter_file_name = parameter_file_name[0]

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

execute_command = f'{executable} {parameter_file_name}'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()
os.system(execute_command)

SubmissionScript.print_runtime()
