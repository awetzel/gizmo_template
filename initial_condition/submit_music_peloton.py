#!/usr/bin/env python3

# peloton standard node: 32 cores, 7.8 GB per core, 250 GB total
#SBATCH --job-name=music
#SBATCH --partition=high2
##SBATCH --partition=high2m    # high-mem node: 32 cores, 15.6 GB per core, 500 GB total
#SBATCH --mem=250G
#SBATCH --nodes=1
#SBATCH --ntasks=1    ## MPI tasks total (using --ntasks-per-node does not work!)
##SBATCH --ntasks-per-node=32    # MPI tasks per node (does not work on peloton!)
#SBATCH --cpus-per-task=32    ## OpenMP threads per MPI task
#SBATCH --time=4:00:00
#SBATCH --output=music_job_%j.txt
#SBATCH --mail-user=arwetzel@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end

'''
Submit a MUSIC job to the SLURM queue.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''


import os
import glob

from utilities import io as ut_io

# names of files
executable = os.environ['HOME'] + '/local/music/MUSIC'
parameter_file_name_base = 'ic_*.conf'

# get whatever parameter file exists in this directory
parameter_file_name = glob.glob(parameter_file_name_base)
assert len(parameter_file_name) == 1
parameter_file_name = parameter_file_name[0]

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

execute_command = f'{executable} {parameter_file_name}'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()
os.system(execute_command)

SubmissionScript.print_runtime()
