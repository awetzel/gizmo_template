#!/usr/bin/env python3.11

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=gizmo
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=4
#SBATCH --tasks-per-node=48    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=48:00:00
#SBATCH --output=gizmo_jobs/gizmo_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin
#SBATCH --account=TG-PHY240075

'''
Submit a Gizmo simulation job to the SLURM queue.
Check if restart files exist - if yes, start a restart run.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io

# whether restart should be from snapshot file
restart_from_snapshot = False

# names of files and directories
executable_file_name = './gizmo/GIZMO'  # executable file
parameter_file_name = 'gizmo_parameters.txt'  # parameter file

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

# check if input id of previous job that needs to finish before this starts
SubmissionScript.check_existing_job('submit_gizmo_stampede.py', submit_before=False, time_delay=4)

# avoid problems with Intel 18 OpenMP
os.environ['KMP_INIT_AT_FORK'] = 'FALSE'

# set execution command
execute_command = f'ibrun mem_affinity {executable_file_name} {parameter_file_name}'
# check if restart
execute_command += SubmissionScript.get_restart_flag(restart_from_snapshot)
# set output files
execute_command += ' 1> gizmo.out 2> gizmo.err'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()

# execute
os.system(execute_command)

SubmissionScript.print_runtime()
