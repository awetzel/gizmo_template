#!/usr/bin/env python3

# job name
#PBS -N gizmo
# node configuration
# select = nodes; ncpus = cores per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Ivy Bridge node: 20 cores, 3.2 GB per core, 64 GB total
#PBS -l select=50:ncpus=20:mpiprocs=10:ompthreads=2:model=ivy
# Haswell node: 24 cores, 5.3 GB per core, 128 GB total
##PBS -l select=50:ncpus=24:mpiprocs=12:ompthreads=2:model=has
# Broadwell node: 28 cores, 4.6 GB per core, 128 GB total
##PBS -l select=100:ncpus=28:mpiprocs=4:ompthreads=7:model=bro
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
##PBS -q devel
##PBS -q normal
#PBS -q long
##PBS -l walltime=2:00:00
##PBS -l walltime=8:00:00
#PBS -l walltime=120:00:00
# combine stderr and stdout into one file
#PBS -j oe
# output file name
##PBS -o gizmo_jobs/gizmo_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -m bae
#PBS -M arwetzel@gmail.com
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit a Gizmo simulation job to the PBS queue.
Check if restart files exist - if yes, start a restart run.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os

from utilities import io as ut_io

# whether should restart from snapshot file
restart_from_snapshot = False

# parallelization parameters
node_number = 50  # number of nodes
mpi_number_per_node = 10  # number of MPI tasks per node

# names of files and directories
executable_file_name = './gizmo/GIZMO'  # executable file
parameter_file_name = 'gizmo_parameters.txt'  # parameter file

# Pleiades settings
os.environ['MPI_DSM_DISTRIBUTE'] = '0'  # better performance
os.environ['KMP_AFFINITY'] = 'disabled'  # prevent machine trying to help & screwing things up

# move to directory am in when submit this job
os.chdir(os.environ['PBS_O_WORKDIR'])

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('pbs', node_number, mpi_number_per_node)

# check if defined/input 'jobid' that needs to finish before this starts
SubmissionScript.check_existing_job('submit_gizmo_pleiades.py', submit_before=False, time_delay=4)

# set execution command
execute_command = 'mpiexec -np {} mbind.x -n{}'.format(
    SubmissionScript.mpi_number, SubmissionScript.mpi_number_per_node
)
# set input files
execute_command += f' {executable_file_name} {parameter_file_name}'
# check if restart
execute_command += SubmissionScript.get_restart_flag(restart_from_snapshot)
# set output files
execute_command += ' 1> gizmo.out 2> gizmo.err'
print(f'executing: {execute_command}\n')
os.sys.stdout.flush()

# execute
os.system(execute_command)

SubmissionScript.print_runtime()
